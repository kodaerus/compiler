#include "parser.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>

//#define DEBUGMODE 1

// prepare for INSANITY!
#define STARTiENUM(name) const char * s ## name [] = {
#define ADDiENUM(type,enm) #enm,
#define FINALiENUM(type,enm) #enm };
#include "tokenenums.h"
#undef STARTiENUM
#undef ADDiENUM
#undef FINALiENUM

const char *operators[] = {
    ",",
    "++",
    "--",
    "==",
    "=",
    ">=",
    "<=",
    "!=",
    "<",
    ">",
    "<<",
    ">>",
    "+",
    "-",
    "*",
    "/",
    "\\",
    "\"",
    "'",
    "+=",
    "-=",
    "*=",
    "/=",
    "&=",
    "|=",
    "<<=",
    ">>=",
    "||=",
    "^=",
    "&",
    "&&",
    "|",
    "||",
    "^",
    "[",
    "]",
    "(",
    ")",
    "{",
    "}",
    ":",
    ";",
    "?",
    ".",
    "->",
    "!",
    "%",
    "%="
};

CParser::CParser(const char *filename) {
    sourceFile = fopen(filename, "r");
}

CParser::~CParser() {
    if (sourceFile != NULL) {
        fclose(sourceFile);
        sourceFile = NULL;
    }
}

int CParser::nextToken(bool enablePreproc) {
    char buf[MAX_PARSER_BUFFER_SIZE]; // means max length is 64
    int buffOffest = 0;
    char readChar = '\0';

    token.type = tok_none;
    token.bytesize = 0;
    token.value.i = 0;

//    while (1) {
        while (1) {
            readChar = fgetc(sourceFile);
            //printf("char read is : '%c'\n", readChar);
            if (readChar == EOF) {
                token.type = tok_EOF;
                return 0;
            }
            buffOffest = 0;
            if (isspace(readChar)) {
                bool preprocPossible = readChar == '\n';
                do {
                    readChar = fgetc(sourceFile);
                    if (readChar == '\n') {
                        preprocPossible = true;
                    }
                } while (readChar != EOF && isspace(readChar));
                if (preprocPossible && readChar == '#') {
                    handlePreprocess();
                } else {
                    ungetc(readChar, sourceFile);
                }
            } else if (enablePreproc && readChar == '#') {
                handlePreprocess();
            } else {
                break;
            }
        }
        if (isdigit(readChar)) {
            bool hasDot = false;
            bool hasExp = false;
            int radix = 10;
            do {
                if (radix == 10 && !hasDot && readChar == '.') {
                    hasDot = true;
                    token.bytesize = 64;
                    buf[buffOffest++] = readChar;
                } else if ((readChar == 'e' || readChar == 'E') && !hasExp && radix == 10) {
                    token.bytesize = 64;
                    hasDot = true;
                    hasExp = true;
                    buf[buffOffest++] = readChar;
                } else if ((readChar == 'x' || readChar == 'X') && buffOffest == 1 && buf[0] == '0') {
                    radix = 16;
                    buf[buffOffest++] = readChar;
                } else if (buffOffest == 0 && readChar == '0') {
                    radix = 8;
                    buf[buffOffest++] = readChar;
                } else if (readChar >= '0' && readChar <= '9' && (int)(readChar-'0') <= radix) {
                    buf[buffOffest++] = readChar;
                } else if (radix > 10 && ((readChar >= 'a' && readChar <= 'f' && (int)(readChar-'a'+10) <= radix) || (readChar >= 'A' && readChar <= 'F' && (int)(readChar-'A'+10) <= radix))) {
                    buf[buffOffest++] = readChar;
                } else {
                    break;
                    //setError("syntax error, unexpected character while reading number");
                    //return 1;
                }
                readChar = fgetc(sourceFile);
            } while (readChar != EOF && (isalnum(readChar) || readChar == '.') && buffOffest < MAX_PARSER_BUFFER_SIZE);
            if (readChar == 'f') {
                if (!hasDot) {
                    setError("Cannot attach 'f' to an integer literal");
                    return 1;
                }
                token.bytesize = 32;
            } else if (readChar == 'l') {
                if (hasDot) {
                    token.bytesize = 64;
                } else {
                    token.bytesize = 32;
                    readChar = fgetc(sourceFile);
                    if (readChar == 'l') {
                        token.bytesize = 64;
                    } else if (readChar != EOF) {
                        ungetc(readChar, sourceFile);
                    }
                }
            } else if (readChar == 'u') {
                if (hasDot) {
                    setError("Cannot have an unsigned float");
                    return 1;
                }
                readChar = fgetc(sourceFile);
                if (readChar == 'l') {
                    token.bytesize = 32;
                    readChar = fgetc(sourceFile);
                    if (readChar == 'l') {
                        token.bytesize = 64;
                    } else if (readChar != EOF) {
                        ungetc(readChar, sourceFile);
                    }
                } else if (readChar != EOF) {
                    ungetc(readChar, sourceFile);
                }
            } else if (readChar != EOF) {
                ungetc(readChar, sourceFile);
            }
            if (token.bytesize == 0) {
                token.bytesize = 32;
            }
            buf[buffOffest] = '\0';
            // some sort of a number
#ifdef DEBUGMODE
            printf("A number %s\n", buf);
#endif
            if (hasDot) {
                token.type = tok_float;
                token.value.f = atof(buf);
                printf("read float: %f\n", token.value.f);
            } else {
                token.type = tok_int;
                token.value.i = strtol(buf, NULL, 0);
                printf("read int: %d\n", (int)token.value.i);
            }
            return 0;
        } else if (isalpha(readChar) || readChar == '_') {
            do {
                buf[buffOffest++] = readChar;
                readChar = fgetc(sourceFile);
            } while (readChar != EOF && (isalnum(readChar) || readChar == '_') && buffOffest < MAX_PARSER_BUFFER_SIZE);
            if (readChar != EOF) {
                ungetc(readChar, sourceFile);
            }
            buf[buffOffest] = '\0';

            // do some keyword checks
            const char **keyClasses[] = {
                spreprocwords,
                sstorageclass,
                stypequalify,
                sdatatypes,
                sconditionkeyword,
                sloopkey,
                sflowcontrol,
                sotherkeyword
            };
            unsigned int keyClassesSize[] = {
                sizeof(spreprocwords)/sizeof(char*),
                sizeof(sstorageclass)/sizeof(char*),
                sizeof(stypequalify)/sizeof(char*),
                sizeof(sdatatypes)/sizeof(char*),
                sizeof(sconditionkeyword)/sizeof(char*),
                sizeof(sloopkey)/sizeof(char*),
                sizeof(sflowcontrol)/sizeof(char*),
                sizeof(sotherkeyword)/sizeof(char*)
            };

            for (unsigned int type = enablePreproc ? 0 : 1; type < sizeof(keyClasses)/sizeof(char**); type++) {
                const char **keyClass = keyClasses[type];
                for (unsigned int i=0; i < keyClassesSize[type]; i++) {
                    if (strcmp(keyClass[i], buf) == 0) {
#ifdef DEBUGMODE
                        printf("found some keyword ID(%d): %s\n", type, buf);
#endif
                        token.type = (tokenType)((int)tok_preprocess+type);
                        token.value.i = i;
                        return 0;
                    }
                }
            }

#ifdef DEBUGMODE
            printf("a string of sorts, possibly an identifier '%s'\n", buf);
#endif
            token.type = tok_identifier;
            strcpy(token.value.s, buf);
            return 0;

            // if that doesn't bring up anything, do some identifier checks
            // finally, blow up in their face
        } else if (!isalnum(readChar) && readChar != '_') {
            do {
                buf[buffOffest++] = readChar;
                buf[buffOffest] = '\0';

                bool found = false;
                for (unsigned int i=0; i < sizeof(operators)/sizeof(char*); i++) {
                    if (strcmp(operators[i], buf) == 0) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    buffOffest--;
                    buf[buffOffest] = '\0';
                    break;
                }

                readChar = fgetc(sourceFile);
            } while (readChar != EOF && (!isalnum(readChar) && readChar != '_') && buffOffest < MAX_PARSER_BUFFER_SIZE);
            if (readChar != EOF) {
                ungetc(readChar, sourceFile);
            }
            buf[buffOffest] = '\0';

            // potentially some sort of an operator
#ifdef DEBUGMODE
            printf("potentially an operator? '%s'\n", buf);
#endif
            token.type = tok_operator;
            strcpy(token.value.s, buf);
            return 0;
        }
//    }

    // technically we should throw an error here for a "WTF IS THIS?!?!"
    printf("unknown char %c", readChar);
    setError("Invalid char encountered");
    return 1;
}

int CParser::readTillChar(char c, bool processEscapes) {
    token.type = tok_string;
    char buf[MAX_PARSER_BUFFER_SIZE];
    char readChar = '\0';
    unsigned int offset = 0;

    while ((readChar = fgetc(sourceFile)) != EOF && readChar != c) {
        buf[offset] = readChar;
        offset++;
    }
    buf[offset] = '\0';
    ungetc(readChar, sourceFile);

    strcpy(token.value.s, buf);

    return 0;
}

int CParser::handlePreprocess() {
    if (!nextToken(true)) {
        if (token.type == tok_preprocess) {
            switch (token.value.preprocKeyword) {
                case ppkw_include:
                    if (!nextToken()) {
                        if (token.type == tok_operator) {
                            if (strcmp(token.value.s, "<") == 0 || strcmp(token.value.s, "\"") == 0) {
                                char c = token.value.s[0];
                                c = c == '<' ? '>' : '"';
                                if (!readTillChar(c, false)) {
                                    printf("include the file %s\n", token.value.s);
                                    nextToken();
                                }
                            } else {
                                setError("SYNTAX ERROR unexpected operator:");
                            }
                        } else {
                            setError("SYNTAX ERROR unexpected token");
                        }
                    }
                    break;
                case ppkw_import:
                    break;
                case ppkw_if:
                    break;
                case ppkw_ifndef:
                    break;
                case ppkw_ifdef:
                    break;
                case ppkw_defined:
                    break;
                case ppkw_define:
                    break;
                case ppkw_elif:
                    break;
                case ppkw_else:
                    break;
                case ppkw_endif:
                    break;
                case ppkw_undef:
                    break;
                case ppkw_line:
                    break;
                case ppkw_error:
                    break;
                case ppkw_pragma:
                    break;
                default:
                    // SYNTAX ERROR
                    break;
            }
        } else {
            // SYNTAX ERROR
        }
    }
    return 0;
}

int CParser::parseFile() {
    if (!sourceFile) {
        setError("Could not load file");
        return 1;
    }

    hasError = false;
    token.type = tok_none;
    token.value.i = 0;

    scopeLevel = 0;

    processBlockStatement(EOF);

    return 0;
}

// Going to need something here to designate what has/has not already been processed
int CParser::parseIdentifierDeclaration(bool hasFirst, bool isParam) {
    edatatypes dataType = dt_void;
    estorageclass storageClass = sc_auto;
    char identifier[MAX_IDENTIFIER_LENGTH];

    // don't really use a single enum for qualifiers
    // since they can have any combination. Can also have const twice
    bool hasType = false;
    bool isConstValue = false;
    bool isConstPtr = false;
    bool isVolatileValue = false;
    bool hasSigned = false;
    bool isSigned = true;
    int pointerLevel = 0;
    bool hasIdent = false;
    bool needsDef = false;
    int arraySize = 0;
    bool isFunction = false;

    if (!hasFirst && nextToken()) {
        setError("Expected a token, none found");
        return 1;
    }
    if (token.type == tok_datatype) {
        dataType = token.value.dataType;
        hasType = true;
    } else if (token.type == tok_typequalifier) {
        switch (token.value.typeQualifier) {
            case tq_const:
                setError("Expected data type before const keyword");
                return 1;
                break;
            case tq_signed:
                isSigned = true;
                hasSigned = true;
                break;
            case tq_unsigned:
                isSigned = false;
                hasSigned = true;
                break;
            case tq_volatile:
                if (isParam) {
                    setError("Cannot have a volatile parameter");
                    return 1;
                }
                isVolatileValue = true;
                break;
            default:
                setError("Type qualifier not handled");
                return 1;
                break;
        }
    } else if (token.type == tok_storageclass) {
        if (isParam) {
            setError("Cannot define storage class for parameter");
            return 1;
        }
        storageClass = token.value.storageClass;
    } else {
        setError("syntax error, expected data type");
        return 1;
    }

    do {
        if (nextToken()) {
            setError("Expected a token, none found");
            return 1;
        }
        if (hasIdent) {
            if (token.type == tok_operator) {
                if (strcmp(token.value.s, "=") == 0) {
                    setError("Not handling variable assignment within declaration yet");
                    return 1;
                } else if (strcmp(token.value.s, "(") == 0) {
                    if (scopeLevel != 0) {
                        setError("Cannot declare a function inside a block");
                        return 1;
                    }
                    isFunction = true;
                    scopeLevel++;
                    do {
                        if (parseIdentifierDeclaration(false, true)) {
                            return 1;
                        }
                    } while (token.type == tok_operator && token.value.s[0] == ',');
                    if (token.type != tok_operator && strcmp(token.value.s, ")")) {
                        setError("syntax error");
                        return 1;
                    }
                    if (nextToken()) {
                        return 1;
                    } else if (token.type == tok_operator) {
                        if (strcmp(token.value.s, "{") == 0) {
                            // load a block!
                            processBlockStatement('}');
                            scopeLevel--;
                            break;
                        } else if (strcmp(token.value.s, ";") == 0) {
                            scopeLevel--;
                            break;
                            // just defining it for now
                        } else {
                            setError("syntax error");
                            return 1;
                        }
                    } else {
                        setError("syntax error");
                        return 1;
                    }
                } else if (strcmp(token.value.s, ")") == 0) {
                    if (isParam) {
                        // ok, we are DONE processing!
                        break;
                    }
                } else if (strcmp(token.value.s, "[") == 0) {
                    if (nextToken()) {
                        return 1;
                    }
                    if (isParam) {
                        if (token.type == tok_operator && strcmp(token.value.s, "]") == 0) {
                            pointerLevel++; // technically, for a parameter. it's just a pointer
                        } else {
                            setError("Expected ']' to end declaration for parameter (no size allowed)");
                            return 1;
                        }
                    } else {
                        if (token.type == tok_int) {
                            needsDef = false;
                            arraySize = token.value.i;
                            if ((nextToken()) || (token.type != tok_operator && strcmp(token.value.s, "]") != 0)) {
                                setError("Syntax error, expected ']'");
                                return 1;
                            }
                        } else if(token.type == tok_identifier) {
                            // technically this needs an identifier lookup and check if constant.
                            setError("array size must be a constant");
                            return 1;
                        } else if(token.type == tok_operator && strcmp(token.value.s, "]") == 0) {
                            needsDef = true; // need definition to have an array size!
                            arraySize = -1;
                        } else {
                            setError("unexpected token while processing array");
                            return 1;
                        }
                    }
                } else if (strcmp(token.value.s, ",") == 0) {
                    if (isParam) {
                        // ok, we are DONE processing!
                        break;
                    } else {
                        // need to load more identifiers!
                    }
                } else if (strcmp(token.value.s, ";") == 0) {
                    if (isParam) {
                        setError("Unexpected ';' while parsing parameter");
                        return 1;
                    } else {
                        // ok, we are DONE processing!
                        break;
                    }
                }
            } else {
                setError("Expected an operator or end of expression");
                return 1;
            }
        } else if (hasType) {
            // if long, then we could have another type, but handle it later
            // could have an operator (*) N-number of times
            // could have an operator '(' which would signify function ptr
            // could have const
            // could have identifier
            if (dataType == dt_long && token.type == tok_datatype) {
                if (token.value.dataType == dt_long) {
                    dataType = dt___longlong;
                } else if (token.value.dataType == dt_long) {
                    dataType = dt___longdouble;
                } else {
                    setError("Invalid data type!");
                    return 1;
                }
            } else if (token.type == tok_operator) {
                if (strcmp(token.value.s, "*") == 0) {
                    pointerLevel++;
                } else if (strcmp(token.value.s, "(") == 0) {
                    setError("Not handling function pointers yet!");
                    return 1;
                } else {
                    setError("Unexpected operator while defining variable");
                    return 1;
                }
            } else if (token.type == tok_typequalifier) {
                if (token.value.typeQualifier == tq_const) {
                    if (pointerLevel == 0) {
                        if (isConstValue) {
                            setError("Already defined as a constant");
                            return 1;
                        } else {
                            isConstValue = true;
                        }
                    } else {
                        if (isConstPtr) {
                            setError("Already defined as a constant");
                            return 1;
                        } else {
                            isConstPtr = true;
                        }
                    }
                } else {
                    setError("Unexpected qualifier after data type declaration");
                    return 1;
                }
            } else if (token.type == tok_identifier) {
                strcpy(identifier, token.value.s);
                hasIdent = true;
            } else {
                setError("Unexpected token");
                return 1;
            }
        } else if (hasSigned) {
            if (token.type == tok_datatype) {
                dataType = token.value.dataType;
                hasType = true;
            }
        } else {
            if (token.type == tok_datatype) {
                dataType = token.value.dataType;
                hasType = true;
            } else if (token.type == tok_typequalifier) {
                switch (token.value.typeQualifier) {
                    case tq_const:
                        setError("Expected data type before const keyword");
                        return 1;
                        break;
                    case tq_signed:
                        isSigned = true;
                        hasSigned = true;
                        break;
                    case tq_unsigned:
                        isSigned = false;
                        hasSigned = true;
                        break;
                    case tq_volatile:
                        isVolatileValue = true;
                        break;
                    default:
                        setError("Type qualifier not handled");
                        return 1;
                        break;
                }
            } else {
                setError("unexpected token");
                return 1;
            }
        }
    } while (1);

    if (needsDef) {
        if (arraySize == -1) {
            setError("Unknown size of array. Please include a size or definition");
            return 1;
        } else {
            setError("variable must be defined");
            return 1;
        }
    }

    printf("Loaded a %s: %s %s %s %s plevel:%d size:%d\n", isFunction ? "function" : "variable", sstorageclass[(int)storageClass], isSigned ? "signed" : "unsigned", sdatatypes[dataType], identifier, pointerLevel, arraySize);

    return 0;
}

int CParser::processBlockStatement(char op) {
    bool EOFflag = false;
    bool firstLine = true;
    while (!EOFflag && !hasError && token.type != tok_EOF) {
        int ret = nextToken(firstLine);
        firstLine = false;

        if (!ret) {
            if (token.type == tok_operator && token.value.s[0] == op) {
                return 0;
            }
            // do some syntax processing!
#ifdef DEBUGMODE
            printf("What kind of token did we get? %d\n", token.type);
#endif
            switch (token.type) {
                case tok_int:
                    break;
                case tok_float:
                    break;
                case tok_string:
                    break;
                case tok_identifier:
                    break;
                case tok_storageclass:
                    break;
    //            case tok_typequalifier:
    //                break;
                case tok_datatype:
                    parseIdentifierDeclaration(true, false);
                    break;
                case tok_conditionkeyword:
                    break;
                case tok_loopkey:
                    break;
                case tok_flowcontrol:
                    break;
                case tok_otherkeyword:
                    break;
                case tok_EOF:
                    EOFflag = true;
                    break;
                default:
                    setError("Unexpected token");
                    break;
            }
        }
        //break;
    }
    return 0;
}

int CParser::processWhileLoop() {
    return 1;
}

int CParser::processDoLoop() {
    return 1;
}

int CParser::processForLoop() {
    return 1;
}

int CParser::processSwitch() {
    return 1;
}

int CParser::processCase() {
    return 1;
}

int CParser::processIf() {
    return 1;
}

int CParser::processElse() {
    return 1;
}

int CParser::processTypedef() {
    return 1;
}

int CParser::processStruct() {
    return 1;
}

int CParser::processEnum() {
    return 1;
}

int CParser::processUnion() {
    return 1;
}

int CParser::calculateSizeof() {
    return 1;
}


int CParser::processStatementTillOperator(char op) {
    return 1;
}

void CParser::setError(const char *error) {
    hasError = true;
    strcpy(errorString, error);
    printf("An error has occured, %s", error);
}

const char* CParser::getError() {
    return errorString;
}
