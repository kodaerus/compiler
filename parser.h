#ifndef PARSER_H
#define PARSER_H

#include <cstdio>

#define MAX_IDENTIFIER_LENGTH 64
#define MAX_PARSER_BUFFER_SIZE MAX_IDENTIFIER_LENGTH+1
//#define MAX_STRING_LENGTH 256

enum tokenType {
    tok_none,
    tok_int,
    tok_float,
    tok_string,
    tok_identifier,
    tok_preprocess,// keywords split into classes
    tok_storageclass,
    tok_typequalifier,
    tok_datatype,
    tok_conditionkeyword,
    tok_loopkey,
    tok_flowcontrol,
    tok_otherkeyword,
    tok_operator, // can i split this up? -, *, & are unary AND binary ops
    tok_EOF
};

#define STARTiENUM(name) enum e ## name {
#define ADDiENUM(type,enm) type ## enm,
#define FINALiENUM(type,enm) type ## enm };
#include "tokenenums.h"
#undef STARTiENUM
#undef ADDiENUM
#undef FINALiENUM

struct TokenInfo {
    tokenType type;
    long bytesize;
    union {
        long long i;
        double f;
        char s[MAX_PARSER_BUFFER_SIZE];
        estorageclass storageClass;
        etypequalify typeQualifier;
        edatatypes dataType;
        econditionkeyword condKeyword;
        eloopkey loopKeyword;
        eflowcontrol flowKeyword;
        eotherkeyword otherKeyword;
        epreprocwords preprocKeyword;
        char oper[8];
    } value;
};

class CParser {
private:
    // so re-adding this back in. thing is,
    // I can use multiple CParser's for each file. DURR.
    FILE *sourceFile;
    bool hasError;
    char errorString[256];
    int scopeLevel;

    // going to need a number of flags like
    // are we following an IF (enable else)
    // following a new line? (enable preprocessor)
    // inside root (enable function dec/defs enum, struct, etc)
public:
    TokenInfo token;
    CParser(const char *filename);
    ~CParser();

    int nextToken(bool enablePreproc = false);
    int readTillChar(char c, bool processEscapes);

    int parseFile();
    int handlePreprocess();

    int parseIdentifierDeclaration(bool hasFirst, bool isParam = false);
    int processBlockStatement(char op);

    int processWhileLoop();
    int processDoLoop();
    int processForLoop();
    int processSwitch();
    int processCase();
    int processIf();
    int processElse(); // not sure if this is needed or if handled by processIf

    int processTypedef();
    int processStruct();
    int processEnum();
    int processUnion();
    int calculateSizeof();

    int processStatementTillOperator(char op);

    void setError(const char *error);
    const char* getError();
};

#endif // PARSER_H
