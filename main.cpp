#include <iostream>
#include "parser.h"

using namespace std;

int main()
{
    // step one. preprocesor
    // step two, parse into AST
    // three, do something to AST
    // moar steps
    // make an output
    CParser parser("testcases/test.c");
    if (parser.parseFile()) {
        printf("Error occured: %s", parser.getError());
    }

    //parseFile("test.c");
    return 0;
}
